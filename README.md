# dino-says-cli

CLI version of dino-says
## Description
Starting from a tweet as usual, I launched this project with a lot of confused intentions 😼<br/>
This is my remix between a tool called cowsay and those tweets 🤷‍♂️


This project uses Gitpod as you just have to click on the below button to start working right from your browser.<br/>
[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://github.com/.../...)

## Usage
For now, just use dinosays.go as it is.<br/>
Further developments are planned to provide binaries and options.

## Authors and acknowledgment


## License
See LICENSE file for details, but spoiler, it's MIT license 🤣

