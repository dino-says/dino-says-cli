package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/dino-says/dino-says-cli/modes"
)

// interactiveCmd represents the interactive command
var interactiveCmd = &cobra.Command{
	Use:   "interactive",
	Short: "A talking dino, but interactive 🤷‍♂️",
	Long:  `Experimentation is the way for a happy geeky life 😉`,
	Run: func(cmd *cobra.Command, args []string) {
		messageFlag, err := cmd.Flags().GetString("message")
		if err != nil {
			fmt.Println("Error trying to read Message flag")
		}
		modes.InteractiveDino(messageFlag)
	},
}

func init() {
	rootCmd.AddCommand(interactiveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// interactiveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// interactiveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
