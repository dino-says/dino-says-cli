package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/dino-says/dino-says-cli/modes"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "dino-says-cli",
	Short: "A dino can speak, believe me",
	Long: `Starting from a tweet as usual, I launched this project with a lot of confused intentions 😼
This is my remix between a tool called cowsay and those tweets 🤷‍♂️`,
	Run: func(cmd *cobra.Command, args []string) {
		messageFlag, err := cmd.Flags().GetString("message")
		if err != nil {
			fmt.Println("Error trying to read Message flag")
		}
		modes.StaticDino(messageFlag)		
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringP("message", "m", "", "message the dino will say")
}
