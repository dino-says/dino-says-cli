package main

import (
	"gitlab.com/dino-says/dino-says-cli/cmd"
)

func main() {
	// All the work is done in a command from Cobra tool
	cmd.Execute()
}
