package dino

import (
	"fmt"
	"strings"
)

type Dino struct {
	BaseModel string
	Message   string
}

func (d *Dino) Show() {
	fmt.Println(d.GetTransformedDino())
}

func (d *Dino) GetTransformedDino() string {
	r := strings.NewReplacer(
		"$message$", d.Message,
	)
	return r.Replace(d.BaseModel)
}
