package modes

import (
	"fmt"

	"gitlab.com/dino-says/dino-says-cli/dino"
	"gitlab.com/dino-says/dino-says-cli/models"
)

func StaticDino(message string) {
	dinoModel, err := models.ModelList.ReadFile("default_dino.model")
	if err != nil {
		fmt.Println("Could not read model file!")
	}
	activeDino := dino.Dino{BaseModel: string(dinoModel), Message: message}
	activeDino.Show()
}
