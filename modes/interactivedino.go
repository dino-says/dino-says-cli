package modes

import (
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/dino-says/dino-says-cli/dino"
	"gitlab.com/dino-says/dino-says-cli/models"
)

type model struct {
	dinoText string
	choices  []string
	selected map[int]struct{}
	cursor   int
}

func (m model) Init() tea.Cmd {
	return nil
}

func initialModel(dinoText string) (m model) {
	m.choices = []string{"Enter message", "Query Dino Wisdom", "Quit"}
	m.selected = make(map[int]struct{})
	m.dinoText = dinoText
	return
}

func (m model) View() string {
	menuText := ""
	for i, choice := range m.choices {
		cursor := " "
		if m.cursor == i {
			cursor = ">"
		}

		checked := " "
		if _, ok := m.selected[i]; ok {
			checked = "x"
		}

		menuText += fmt.Sprintf("%s [%s] %s\n", cursor, checked, choice)
	}

	s := lipgloss.JoinHorizontal(lipgloss.Left, m.dinoText, menuText)

	s += "\n\nPress q to quit.\n"
	return s
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		case "up", "k":
			if m.cursor > 0 {
				m.cursor--
			}
		case "down", "j":
			if m.cursor < len(m.choices)-1 {
				m.cursor++
			}
		case "enter", " ":
			_, ok := m.selected[m.cursor]
			if ok {
				delete(m.selected, m.cursor)
			} else {
				m.selected[m.cursor] = struct{}{}
			}
		}
	}

	return m, nil
}

func InteractiveDino(message string) {
	dinoModel, err := models.ModelList.ReadFile("default_dino.model")
	if err != nil {
		fmt.Println("Could not read model file!")
	}
	activeDino := dino.Dino{BaseModel: string(dinoModel), Message: message}

	p := tea.NewProgram(initialModel(activeDino.GetTransformedDino()), tea.WithAltScreen())
	if err := p.Start(); err != nil {
		fmt.Printf("Alas, there's been an error: %v", err)
		os.Exit(1)
	}
}
